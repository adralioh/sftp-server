ARG ALPINE_VERSION=latest
FROM alpine:$ALPINE_VERSION AS prod

RUN apk add --no-cache openssh-server

COPY start-container /usr/local/sbin/
COPY sshd_config /etc/ssh/
RUN chmod 0755 /usr/local/sbin/start-container && \
    chmod 0644 /etc/ssh/sshd_config

EXPOSE 22
VOLUME /data

CMD ["/usr/local/sbin/start-container"]


# Add the test script to the image
# Build via `docker build --target=test`
FROM prod AS test
COPY run-tests /usr/local/sbin/
RUN chmod 0755 /usr/local/sbin/run-tests
CMD ["/usr/local/sbin/run-tests"]

# Default to the prod stage when `--target` isn't given
FROM prod
