# sftp-server
SFTP server in a container

## Features
- Supports chroot jails. By default, all users are jailed to the same dir
- Supports creating users and groups, as well as modifying the sshd config when the container is run. No need to build a new image

## Registry
Prebuilt images are available from `registry.gitlab.com/adralioh/sftp-server`

Supported tags:
- `latest`, `alpine`, `alpine-3`, `alpine-3.13`
- `alpine-3.12`

## Usage
- Command-line
  ```bash
  docker run --name sftp-server -p 2222:22 \
             -v /local/config:/config -v /local/data:/data \
             registry.gitlab.com/adralioh/sftp-server:latest
  ```

- Docker compose
  ```yaml
  ---
  version: '2.0'

  services:
    sftp-server:
      image: registry.gitlab.com/adralioh/sftp-server:latest
      ports:
        - '2222:22'
      volumes:
        - config:/config
        - data:/data

  volumes:
    config:
    data:
  ```

- Connecting to the container via sftp

  Users and ssh keys must be defined first. See [Configuration](#configuration)
  ```bash
  sftp -P 2222 -i ssh_key user@server
  ```

## Configuration
Data is stored at `/data`, and all users are jailed to this dir by default

Configuration is stored at `/config`. Running the container once will populate it
```
/config
├── sshd_config_override
├── users
├── groups
├── authorized_keys
│   ├── user1
│   ├── user2
│   └── user3
└── host_keys
    ├── ssh_host_ecdsa_key
    ├── ssh_host_ecdsa_key.pub
    ├── ssh_host_ed25519_key
    ├── ssh_host_ed25519_key.pub
    ├── ssh_host_rsa_key
    └── ssh_host_rsa_key.pub
```

- `sshd_config_override`

  This file is included at the top of `sshd_config`. You can define custom settings here

- `users` and `groups`

  These files contain the users and groups that will be created when the container starts

- `authorized_keys/USERNAME`

  Authorized ssh keys for the given user. Password authentication is disabled, so you must define public keys here in order to log in

- `host_keys/`

  SSH server host keys

## Testing
Basic tests are provided that ensure the container doesn't crash on startup

1.  Build the testing image
    ```bash
    docker build --target=test -t sftp-server-test:latest .
    ```
1.  Run the tests
    ```bash
    docker run --rm sftp-server-test:latest
    ```
